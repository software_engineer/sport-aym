<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Team_model extends CI_Model {

    /* public function __construct()
    {
            parent::__construct();
            // Your own constructor code
    } */
    public $teamImagesPath =  __DIR__.'\..\..\assets\images\teams-players\\';
    
    public function imageHandling($mode,$data){
        $return = null;
        
        $uploaddir = __DIR__.'\..\..\assets\images\teams-players\\';

        if ($mode   ==  "add") {
            $newName = date("dmYHis")."_".basename($data['badge']['name']);

            move_uploaded_file($data['badge']['tmp_name'], $this->teamImagesPath.$newName);
            $return = $newName;
        }

        /* if (file_exists($uploadfile)){
            $return = basename($data['badge']['name']);
        }else{
        } */
        
        return $return;
    }

    public function validateNameUnique($name){
        $query = $this->db->query("select * from tbl_teams where `name` = '".trim($name)."' ");//echo"<pre>";print_r($query->result_array()[0]);exit();
        return $query->result_array();
    }

    public function register($post,$files,$captainId)
    {   
        $input=[
            "name"              =>$post["teamname"],
            "location_name"     =>$post["location_name"],
            "location_district" =>$post["location_district"],
            "location_state"    =>$post["location_state"],
            "location_country"  =>$post["location_country"],
            "location_pincode"  =>$post["location_pincode"],
            "created_by"        =>1
        ];

        $fileName = $this->imageHandling("add",$_FILES);
        if ($fileName !=="") {
            $input["img_name"] = $fileName;
        }

        $query = $this->db->insert('tbl_teams', $input);
        $tblTeamsId = $this->db->insert_id();

        $tbl_team_players=[
            "team_id"       =>  $tblTeamsId,
            "player_id"     =>  $captainId,
            "is_captain"    =>  1,
            "created_by"    =>  1
        ];
        $tblTeamPlayers = $this->db->insert('tbl_team_players', $tbl_team_players);
        $tblTeamPlayersid = $this->db->insert_id();
        
        $return=[
            "team"      =>   $tblTeamsId,
            "player"    =>   $tblTeamPlayersid,
        ];

        return $return;
    }

    public function getList()
    {
        $query = $this->db->query("select * from tbl_teams");

        $teams=[];
        foreach ($query->result_array() as $key => $team) {
           $teams[]=[
               "id"         =>  $team["id"],
               "name"       =>  $team["name"],
               "location_name"   =>  $team["location_name"],
               "img_logo"   =>  $team["img_name"],
               /* "img_banner" =>  $team["img_banner"], */
           ];
        }
        return $teams;
        //echo"<pre>";print_r();exit();
    }

    public function details($id)
    {
        $query = $this->db->query("select * from tbl_teams where `id` =".$id);//echo"<pre>";print_r($query->result_array()[0]);exit();
        return $query->result_array()[0];
    }

    public function players($teamId){
        $querystring = "select 
        ttp.id as team_player_id,
        tp.id as person_player_id,
        ttp.is_captain,
        tp.f_name,
        tp.l_name,
        tp.contact,
        tp.email,
        tp.dob,
        tp.image_path
        from `tbl_team_players` as ttp
        left join `tbl_person` as tp on ttp.player_id = tp.id
        where ttp.team_id = ".$teamId;

        $query=$this->db->query($querystring);
        return $query->result_array();

        //echo "<pre>";print_r($teamId);exit();
    }
}

?>