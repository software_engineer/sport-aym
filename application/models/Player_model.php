<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Player_model extends CI_Model {

    /* public function __construct()
    {
            parent::__construct();
            // Your own constructor code
    } */

    public function validateContact($contact){
        $query = $this->db->query("select * from `tbl_person` where contact ='".trim($contact)."' ");//echo $this->db->last_query();exit();
        return $query->result_array();
    }

    public function validateEmail($emailId){
        $query = $this->db->query("select * from `tbl_person` where email ='".trim($emailId)."' ");//echo $this->db->last_query();exit();
        return $query->result_array();
    }

    public function register($data)
    {
        //echo"<pre>";print_r($data);exit();
        $response=[];

        $personFName = $data["fname"];
        $personLName = $data["lname"];
        $personContact = $data["contact"];
        $personEmail = $data["email"];
        $personDob = $data["dob"];

        $teamId = $data["tid"];
        
        $tblPersonInput=[
            "f_name"=>$personFName,
            "l_name"=>$personLName,
            "contact"=>$personContact,
            "email"=>$personEmail,
            "dob"=>$personDob,
            "image_path"=>"",
            "person_type"=>3,
            "created_by"=>3,
        ];
        $this->db->insert('tbl_person', $tblPersonInput);
        $personId = $this->db->insert_id();
        $response["tbl_person"] = $personId;

        $teamPlayer=[
            "team_id" => $teamId,
            "player_id" => $personId,
            "created_by" => 1,
        ];

        //unmark existing Captain and set current as Captain
        if (isset($data["team_admin"])) {
            $query = $this->db->query("UPDATE tbl_team_players SET `is_captain` = 0 WHERE team_id = ".$teamId);
            $teamPlayer["is_captain"] = 1;
        }

        
        $this->db->insert('tbl_team_players', $teamPlayer);
        $response["tbl_teams"] = $teamInputId = $this->db->insert_id();

        return $response;
        /* $query = $this->db->get('entries', 10);
        return $query->result(); */
    }

    public function registerCaptain($data)
    {
        $tblPersonInput=[
            "f_name"=>$data["fname"],
            "l_name"=>$data["lname"],
            "contact"=>$data["contact"],
            "email"=>$data["email"],
            "dob"=>$data["dob"],
            "person_type"=>"3",
            "created_by"=>"1"
        ];
        $this->db->insert('tbl_person', $tblPersonInput);
        return $this->db->insert_id();
    }

    public function update($data){

        $response["success"] = 0;
        $updatePerson="";
        $updateTeamPlayerTable="";

        $querystring = "select 
        ttp.id as team_player_id,
        tp.id as person_player_id,
        ttp.is_captain,
        tp.f_name,
        tp.l_name,
        tp.contact,
        tp.email,
        tp.dob,
        tp.image_path
        from `tbl_team_players` as ttp
        left join `tbl_person` as tp on ttp.player_id = tp.id
        where tp.id=".$data["pid"]."
        AND ttp.team_id = ".$data["tid"]."
        AND ttp.id = ".$data["ttpid"];
        $query=$this->db->query($querystring);
        $dbData = $query->result_array()[0];

        $incomingPersonData=[
            "f_name" => $data["fname"],
            "l_name" => $data["lname"],
            "contact" => $data["contact"],
            "email" => $data["email"],
            "dob" => $data["dob"],
            "image_path" => ""
        ];

        $diffPerson = array_diff_assoc($incomingPersonData,$dbData);

        if (!empty($diffPerson)) 
        {
            $this->db->update('tbl_person', $diffPerson, array('id' => $data["pid"]));
            $updatePerson = $this->db->affected_rows();
        }

        $tblTeamInsert=[];
        if (array_key_exists("team_admin",$data)) {
            $tblTeamInsert["is_captain"] = 1;
        }

        if (!empty($tblTeamInsert)) 
        {
            //unset existing captain
            $this->db->update('tbl_team_players', array("is_captain"=>0), array('team_id' => $data["tid"]));

            //set new captain
            $this->db->update('tbl_team_players', $tblTeamInsert, array('id' => $data["ttpid"]));
            $updateTeamPlayerTable = $this->db->affected_rows();
        }

        if($updatePerson || $updateTeamPlayerTable){
            $response["success"] = 1;
            $response["data"] =[
                "updatePerson" => $updatePerson,
                "updateTeamPlayerTable" => $updateTeamPlayerTable,
            ];
        }

        return $response;
    }
}

?>