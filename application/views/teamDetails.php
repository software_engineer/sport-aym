
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
            <div class="team-page-head-content" style="">
                <div class="team-detail-badge" style="background-image:url('<?php echo base_url()."assets/images/teams-players/".$team["img_name"]; ?>');">
                </div>
                <div class="team-detail-content" style="">
                    <div class="text-detail" style="">
                        <ul>
                            <input type="hidden" id="teamid" name="teamid" value="<?php echo $team["id"]; ?>" />
                            <li><?php echo $team["name"]; ?></li>
                            <li><?php echo $team["location_name"].", ".$team["location_district"]; ?></li>
                            <li>Registered on <?php echo date("jS F Y",strtotime($team["created_on"])); ?></li>
                        </ul>
                    </div>
                    <div class="button-holder" style="">
                        <button title="Edit" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalPlayerDetails">
                            <i class="bi bi-pencil" data-playerid="" ></i> Team
                        </button>
                        <button title="Add" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalPlayerDetails">
                            <i class="bi bi-person-plus" data-playerid="" ></i> Player
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="stats-tab" data-toggle="tab" href="#stats" role="tab" aria-controls="contact" aria-selected="false">Stats</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="squad-tab" data-toggle="tab" href="#squad" role="tab" aria-controls="home" aria-selected="true">Squad</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade col-md-12 tab-container-style show active" id="stats" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="stats-pwld-container-p1" style="">
                        <div class="stats-pwld-container" style="">
                            <div class="set-of-3" >
                                <div class="stat-container">
                                    <label class="stat-title">Played</label>
                                    <label class="stat-value">1,119</label>
                                </div>
                                <div class="stat-container">
                                    <label class="stat-title">Wins</label>
                                    <label class="stat-value">599</label>
                                </div>
                                <div class="stat-container">
                                    <label class="stat-title">Losses</label>
                                    <label class="stat-value">239</label>
                                </div>
                            </div>
                            <div class="set-of-3" >
                                <div class="stat-container">
                                    <label class="stat-title">Goals</label>
                                    <label class="stat-value">1,958</label>
                                </div>
                                <div class="stat-container">
                                    <label class="stat-title">Goals Conceded</label>
                                    <label class="stat-value">1,109</label>
                                </div>
                                <div class="stat-container">
                                    <label class="stat-title">Clean Sheets</label>
                                    <label class="stat-value">425</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
                <div class="tab-container-style tab-pane fade " id="squad" role="tabpanel" aria-labelledby="home-tab">  
                        <ul class="playerListContainer card-list">
                            <li class=""><div class="playerOverviewCard"><header class="squadPlayerHeader"><span class="playerCardInfo"><span class="number"></span><h4 class="name">Jenifer Aniston</h4><span class="position"></span></span><div class="player-image" style="background-image:url('https://resources.premierleague.com/premierleague/photos/players/110x140/p80201.png');"></div></header><ul class="squadPlayerStats"><li><dl><dt class="label">Played</dt><dd class="info"></dd></dl></li><li><dl><dt class="label">Age</dt><dd class="info"> yrs.</dd></dl></li></ul><div class="modify-player" style=""><i class="bi bi-pencil-fill"></i><i class="bi bi-trash-fill"></i></div></div></li><li class=""><div class="playerOverviewCard"><header class="squadPlayerHeader"><span class="playerCardInfo"><span class="number"></span><h4 class="name">Jenna Marbles</h4><span class="position"></span></span><div class="player-image" style="background-image:url('https://resources.premierleague.com/premierleague/photos/players/110x140/p80201.png');"></div></header><ul class="squadPlayerStats"><li><dl><dt class="label">Played</dt><dd class="info"></dd></dl></li><li><dl><dt class="label">Age</dt><dd class="info"> yrs.</dd></dl></li></ul><div class="modify-player" style=""><i class="bi bi-pencil-fill"></i><i class="bi bi-trash-fill"></i></div></div></li><li class=""><div class="playerOverviewCard"><header class="squadPlayerHeader"><span class="playerCardInfo"><span class="number"></span><h4 class="name">Jimmy Carter</h4><span class="position"></span></span><div class="player-image" style="background-image:url('https://resources.premierleague.com/premierleague/photos/players/110x140/p80201.png');"></div></header><ul class="squadPlayerStats"><li><dl><dt class="label">Played</dt><dd class="info"></dd></dl></li><li><dl><dt class="label">Age</dt><dd class="info"> yrs.</dd></dl></li></ul><div class="modify-player" style=""><i class="bi bi-pencil-fill"></i><i class="bi bi-trash-fill"></i></div></div></li><li class=""><div class="playerOverviewCard"><header class="squadPlayerHeader"><span class="playerCardInfo"><span class="number"></span><h4 class="name">Amy Winehouse</h4><span class="position"></span></span><div class="player-image" style="background-image:url('https://resources.premierleague.com/premierleague/photos/players/110x140/p80201.png');"></div></header><ul class="squadPlayerStats"><li><dl><dt class="label">Played</dt><dd class="info"></dd></dl></li><li><dl><dt class="label">Age</dt><dd class="info"> yrs.</dd></dl></li></ul><div class="modify-player" style=""><i class="bi bi-pencil-fill"></i><i class="bi bi-trash-fill"></i></div></div></li>
                        </ul>
                    </div>
                </div>
        </div>
<!-- Modal -->
<div id="modalPlayerDetails" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Player Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" id="playerForm" action="Team/registerPlayer" method="post">
            <input type="hidden" id="pid" name="pid" value="" />
            <input type="hidden" id="ttpid" name="ttpid" value="" />
            <input type="hidden" id="tid" name="tid" value="" />
            <div class="form-row">
                <div class="form-group col">
                    <label for="registerfName" class="requiredInput">First Name</label>
                    <input type="text" class="form-control personName" id="registerfName" aria-describedby=""  name="fname" placeholder="Name">
                </div>
                <div class="form-group col">
                    <label for="registerlName">Last Name</label>
                    <input type="text" class="form-control personName" id="registerlName" aria-describedby=""  name="lname" placeholder="Surname">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col">
                    <label for="registerEmail" class="requiredInput">Email</label>
                    <input type="email" class="form-control onBlurEmailValidate" id="registerEmail" aria-describedby=""  name="email" placeholder="Enter Email Id">
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="registerContact" class="requiredInput">Contact Number</label>
                        <input type="text" class="form-control onBlurContactValidate keyupNumeric" id="registerContact" aria-describedby=""  name="contact" placeholder="Enter Number">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="registerYearOfBirth">Year Of Birth</label>
                        <input type="text" class="form-control keyupNumeric numeric-year" id="registerYearOfBirth" aria-describedby=""  min="" max="" name="dob" placeholder="Enter Year">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group" style="text-align: center;padding-top: 33px;">
                        <input type="checkbox" name="team_admin" class="form-check-input" id="teamAdmin" name="admin" >
                        <label class="form-check-label" for="teamAdmin">Team Captain / Manager</label>
                    </div>
                </div>
                <div class="col">

                </div>
            </div>
            <div class="form-row" style="display:none;">  
                <div class="form-group col">
                    <button type="submit" class="btn btn-primary">Register</button>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary submitPlayerForm">Register</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>