        <div class="col-md-12" style="height:unset;">
            <div>
                <h1 class="display-4">Teams</h1>
                <h6>List of teams that have been registered.</h6>
            </div>
            <div style="display: flex;">
                <div class="col-md-10" style="">
                    <div class="input-group" style="width: 40%;margin-left:auto;">
                        <input type="text" class="form-control" aria-label="">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="bi bi-search"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="">
                    <button title="Add" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalTeamDetails">
                        <i class="bi bi-plus"></i>
                        <i class="bi bi-people" data-playerid="" ></i> Team
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <ul id="listOfTeams" class="card-list">
            </ul>
        </div>

<!-- Modal -->
<div id="modalTeamDetails" class="modal modal-fullscreen fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">New Team</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" id="createTeamForm" action="Team/register" method="post">
            <input type="hidden" name="id" value="" />
            <input type="hidden" name="tid" value="" />
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="registerPinCode">Team Badge</label>
                        <div id="teamBadgeUpload" style="" class="previewImage">
                            <input type="file" style="" class="form-control previewSelectedImage" id="registerbadge" accept="image/png, image/jpeg" aria-describedby=""  name="badge" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="registerName" class="requiredInput">Team Name</label>
                        <input type="text" class="form-control onBlurTeamNameUniqueValidate" id="registerTeamName" aria-describedby=""  name="teamname" placeholder="Enter Team Name" required>
                    </div>
                    <div class="form-group">
                        <label for="registerPinCode" class="requiredInput">Area Pin</label>
                        <input type="text" class="form-control teamLocation onBlurPinCodeValidate" id="registerPinCode" attr-target="#registerAreaForPin" name="location_pincode" aria-describedby="" placeholder="Enter Pin Code" required>
                        <input type="hidden" class="teamLocation locHidden" id="locName" aria-describedby=""  name="location_name" >
                        <input type="hidden" class="teamLocation locHidden" id="locDistrict" aria-describedby=""  name="location_district" >
                        <input type="hidden" class="teamLocation locHidden" id="locState" aria-describedby=""  name="location_state" >
                        <input type="hidden" class="teamLocation locHidden" id="locCountry" aria-describedby=""  name="location_country" >
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="registerName" class="requiredInput">Cpt. First Name</label>
                        <input type="text" class="form-control" id="registerName" aria-describedby=""  name="fname" placeholder="Enter First Name" required>
                    </div>
                    <div class="form-group">
                        <label for="registerName" class="requiredInput">Cpt. Last Name</label>
                        <input type="text" class="form-control" id="registerName" aria-describedby=""  name="lname" placeholder="Enter Last Name" required>
                    </div>
                    <div class="form-group">
                        <label for="registerDob" class="requiredInput">Cpt. Year of Birth</label>
                        <input type="text" class="form-control keyupNumeric numeric-year" id="registerDob" aria-describedby=""  name="dob" placeholder="Enter Year of birth" min="1990" max="2021" step="1" required>
                    </div>
                    <div class="form-group has-validation">
                        <label for="registerEmail" class="requiredInput">Cpt. Email</label>
                        <input type="email" class="form-control onBlurEmailValidate" id="registerEmail" aria-describedby="registerEmailFeedback"  name="email" placeholder="Enter Email Id" required>
                    </div>
                    <div class="form-group has-validation">
                        <label for="registerContact" class="requiredInput">Cpt. Contact</label>
                        <input type="text" class="form-control onBlurContactValidate" id="registerContact" aria-describedby="registerContactFeedback"  name="contact" placeholder="Enter Phone number" required>
                    </div>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary submitTeamForm">Register</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>