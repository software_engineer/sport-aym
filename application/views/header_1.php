<!doctype html>
<html lang="en">
	<head>
        <meta charset="utf-8">
        <meta name="theme-color" content="#e6e6e6">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="title" content="Sport Bar">
        <meta name="description" content="Sport Event Management Portal">
        <meta name="author" content="Lohit Pereira"> 

		<title>Sport Bar</title>
        
        <base href="<?php echo base_url(); ?>" target="">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" >
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-grid.min.css" >
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-reboot.min.css" >
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
        <?php 
            if (isset($css)) {
                foreach ($css as $key => $value) {
                    if ($value != '') {
                        echo '<link rel="stylesheet" href="'.base_url().$value.'">';
                    }
                }
            }
        ?>
    </head>
    <body>
        <div class="container-fluid" style="padding-left:0px">
            <div id="sidebar" class="sidebar-compact sidebarDesktopToggle" style="">
                <div class="sidebar-brand-detail">
                    <img alt="AYM Logo" src="assets/images/app/logo-football.png" width="" height="" ><!-- style="height: 70px;" -->
                    <label>AYM Sports</label>
                </div>
                <i class="bi bi-list mobile-menu-button sidebarToggleMenu"></i>
                <ul class="sidebar-list-container">
                    <li class="sidebar-toggle-li" >
                        <a href="<?php echo base_url(); ?>Tournaments">
                            <i class="bi bi-calendar2-event"></i>
                            <label>Tournaments</label>
                        </a>
                    </li>
                    <li class="sidebar-toggle-li">
                        <a href="<?php echo base_url(); ?>Team">
                            <i class="bi bi-people"></i>
                            <label>Teams</label>
                        </a>
                    </li>
                    <li class="sidebar-toggle-li">
                        <a href="<?php echo base_url(); ?>Activities">
                            <i class="bi bi-x-diamond"></i>
                            <label>Activities</label>
                        </a>
                    </li>
                    <li class="sidebar-toggle-li">
                        <a href="<?php echo base_url(); ?>Users">
                            <i class="bi bi-person-bounding-box"></i>
                            <label>Users</label>
                        </a>
                    </li>
                    <li class="sidebar-toggle-li">
                        <a href="<?php echo base_url(); ?>Settings">
                            <i class="bi bi-gear"></i>
                            <label>Settings</label>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="row content-compact" style="">
