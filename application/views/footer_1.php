             </div>
        </div>
        <script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js" ></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js" ></script>
        <?php 
            if (isset($js)) {
                foreach ($js as $key => $value) {
                    if ($value != '') {
                        echo '<script src="'.base_url().$value.'" ></script>';
                    }
                }
            }
        ?>
    </body>
</html>