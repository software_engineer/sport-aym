<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Team extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
        parent::__construct();

		$this->load->model("Team_model");
		$this->load->model("player_model");
    }

	public function index()
	{
		$data=[
			"css"=>[
				"assets/css/master.css",
				"assets/css/module-team.css"
			],
			"js"=>[
				"assets/js/json2html-master/json2html.js",
				"assets/js/json2html-master/jquery.json2html.js",
				"assets/js/jquery.form.min.js",
				"assets/js/master.js",
				"assets/js/master-validation.js",
				"assets/js/team.js"
			]
		];
		
		$this->load->view('header_1',$data);
		$this->load->view('team',$data);
		$this->load->view('footer_1',$data);
	}

	public function details($id)
	{
		$data=[
			"css"=>[
				"assets/css/master.css",
				"assets/css/module-teamDetails.css"
			],
			"js"=>[
				"assets/js/json2html-master/json2html.js",
				"assets/js/json2html-master/jquery.json2html.js",
				"assets/js/jquery.form.min.js",
				"assets/js/master.js",
				"assets/js/master-validation.js",
				"assets/js/teamDetails.js"
			]
		];
		$data["team"]	=	$this->Team_model->details($id);
		
		$this->load->view('header_1',$data);
		$this->load->view('teamDetails',$data);
		$this->load->view('footer_1',$data);
	}

	public function fetchDetails($id)
	{
		$response["success"] = 0;
		$data = $this->Team_model->fetchDetails($id);
		if($data){
			$response=[
				"success" 	=> 1,
				"data" 		=> $data
			];
		}
		echo json_encode($response);
	}

	public function validateNameUnique(){
		$response["success"] = 0;

		$playerData = $this->Team_model->validateNameUnique($_POST["name"]);
		if ($playerData) {
			$response["success"] = 1;
			//$response["data"] = $playerData;
		}

		echo json_encode($response,true);
	}

	public function register(){

		$response = [
			"success"	=>	1
		];
		
		$captainId = $this->player_model->registerCaptain($_POST);

		if ($captainId > 0 ) {
			$teamId = $this->Team_model->register($_POST,$_FILES,$captainId);
			$response["success"] = "1";
			$response["message"] = $teamId;
		}

		echo json_encode($response);
	}

	public function registerPlayer()
	{	
		
		$response["success"] = 0;

		if ($this->input->post("pid")) {
			$response = $this->player_model->update($this->input->post());
		} else {
			$modelResponse = $this->player_model->register($this->input->post());
			
			if ($modelResponse) {
				$response=[
					"success" => 1,
					"data"	=> $modelResponse
				];
			}
		}
		
		echo json_encode($response);
	}

	public function getList(){
		$response["success"] = 0;

		$list = $this->Team_model->getList();

		if ($list) {
			$response=[
				"success" => 1,
				"data"	=> $list
			];
		}

		echo json_encode($response);
	}

	public function players(){
		$response["success"] = 0;

		$list = $this->Team_model->players($this->input->post("tid"));

		if ($list) {
			$response=[
				"success" => 1,
				"data"	=> $list
			];
		}

		echo json_encode($response);
	}
}
