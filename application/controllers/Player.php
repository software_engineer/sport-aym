<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Player extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
        parent::__construct();
		$this->load->helper('url');
		
		$this->load->model("player_model");
    }

	public function validateContact(){
		$response["success"] = 0;

		$playerData = $this->player_model->validateContact($_POST["contact"]);
		if ($playerData) {
			$response["success"] = 1;
			//$response["data"] = $playerData;
		}

		echo json_encode($response,true);
	}

	public function validateEmail(){
		$response["success"] = 0;

		$playerData = $this->player_model->validateEmail($_POST["email"]);
		if ($playerData) {
			$response["success"] = 1;
			//$response["data"] = $playerData;
		}

		echo json_encode($response,true);
	}
	public function register()
	{	
		pre($_POST);exit();
	}
}
