$(document).on("mouseover",".sidebar-compact.sidebarDesktopToggle",function(){
    $(".sidebar-brand-detail > label").show();
    $(".sidebar-list-container label").show();
    $(".sidebar-toggle-li").addClass("sidebar-toggle-li-open");
});

$(document).on("mouseout",".sidebar-compact.sidebarDesktopToggle",function(){
    $(".sidebar-brand-detail > label").hide();
    $(".sidebar-list-container label").hide();
    $(".sidebar-toggle-li").removeClass("sidebar-toggle-li-open");
});

$(document).on("click",".sidebarToggleMenu",function(){
    var element = $(this);
    if (element.hasClass('bi-list')) {
        element.removeClass('bi-list').addClass('bi-x');
        $(".sidebar-compact").addClass('mobile-nav-open');
    }else{
        element.removeClass('bi-x').addClass('bi-list');
        $(".sidebar-compact").removeClass('mobile-nav-open');
    }
});

// image upload on div click
$(document).on("change",".previewSelectedImage",function(){
    let image = URL.createObjectURL(this.files[0]);
    $(this).parent().css({"background-image":"url('"+image+"')"});
});

$('.personName').on("keyup",function(event) {
    var textBox = event.target;
    var start = textBox.selectionStart;
    var end = textBox.selectionEnd;
    textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1).toLowerCase();
    textBox.setSelectionRange(start, end);
});