$(function(){
    loadTeamList(false);
});

function loadTeamList(isScrolling){
    
    //check filters if any

    //fetch
    $.get($("base").attr("href")+"Team/getList",
    function(data){
        var response = JSON.parse(data);

        var t_teamList={"<>":"li","html":[
            {"<>":"a","class":"team-card","href":"Team/${id}","html":[
                {"<>":"div","class":"team-banner","style":"background:url('https://i.ibb.co/SrgMv3r/laptop-coffee.jpg');","html":""},
                {"<>":"div","class":"team-badge","style":function($object,index){
                    var backgroundImage = $("base").attr("href")+"assets/images/teams-players/"+$object.img_logo;
                    return "background:url('"+backgroundImage+"')";
                },"html":""},
                {"<>":"div","class":"team-info","html":[
                    {"<>":"h5","html":"${name}"},
                    {"<>":"h6","html":"${location_name}"}
                ]}
            ]}
        ]};

        if (response.success === 1) {
            $("#listOfTeams").json2html(response.data,t_teamList);
        }
    });
}

$(document).on("click",".submitTeamForm",function(){
    let valid = validateTeamCreationForm();
    if(valid){
        submitPlayerForm();
    }
});

function validateTeamCreationForm(){

    var finalReturn = true;
    
    $("#createTeamForm input[required='']").each(function(){
        if (
            $(this).val() === ""    ||
            $(this).hasClass("is-invalid") === true
            ) {

                if ($(this).val() === "") {
                    $(this).addClass("is-invalid");
                }

            finalReturn = false;
        }
    });

    return finalReturn;
}

function submitPlayerForm(){
    $("#createTeamForm").ajaxSubmit({
        success:    function(data) {
            var response = JSON.parse(data);
            if(response.success === "1"){
                loadTeamList(false);
                $('#modalTeamDetails').modal('hide');
                $('#createTeamForm').resetForm();
                $('#createTeamForm').clearForm();
            }
        }
    });
    
}

$('#modalTeamDetails').on('hide.bs.modal', function (e) {
    $(".previewImage").attr("style","");
    $('#modalTeamDetails input').val("").removeClass("is-valid is-invalid").prop("checked",false);

    $('#modalTeamDetails select').prop("disabled",false);
    $('#modalTeamDetails option:first').prop('selected',true);
});

/* $(document).on('click','.editTeam',function(){
    var thisTeam = $(this);
    $('#modalTeamDetails #id').val(thisTeam.attr('data-id'));
    $('#modalTeamDetails #tid').val(thisTeam.attr('data-id'));
    $('#modalTeamDetails #registerName').val(thisTeam.attr('data-name'));
    $('#modalTeamDetails #registerContact').val(thisTeam.attr('data-contact'));
    $('#modalTeamDetails #registerEmail').val(thisTeam.attr('data-email'));

    $('#modalTeamDetails select').prop("disabled",true);
    $('#modalTeamDetails #registerTeamSelect').val(thisPlayer.attr('data-teamid'));

    $('#modalTeamDetails #teamAdmin').prop('checked',parseInt(thisPlayer.attr('data-teamadmin')));
}); */