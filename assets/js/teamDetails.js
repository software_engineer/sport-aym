$(function(){
    loadPlayerList(false);
});

$(document).on("click",".submitPlayerForm",function(){
    let valid = validateTeamCreationForm();
    if(valid){
        submitPlayerForm();
    }
});

function validateTeamCreationForm(){

    return true;
}

function submitPlayerForm(){
    $("#playerForm").ajaxSubmit({
        success:    function(data) {
            var response = JSON.parse(data);
            if(response.success === 1){
                loadPlayerList(false);console.log(response.success);
                $('#modalPlayerDetails').modal('hide');
                $('#modalPlayerDetails').resetForm();
                $('#modalPlayerDetails').clearForm();
            }
        }
    });
}

var t_playerCard={"<>":"li","class":"","html":[
    {"<>":"div","class":"playerOverviewCard","html":[
        {"<>":"header","class":"squadPlayerHeader","html":[
            {"<>":"span","class":"playerCardInfo","html":[
                {"<>":"span","class":"number","html":"${person_id}"},
                {"<>":"h4","class":"name","html":"${f_name} ${l_name}"},
                {"<>":"span","class":"position","html":"${position}"}
              ]},
            {"<>":"div","class":"player-image","style":"background-image:url('https://resources.premierleague.com/premierleague/photos/players/110x140/p80201.png');","html":""}
          ]},
        {"<>":"ul","class":"squadPlayerStats","html":[
            {"<>":"li","html":[
                {"<>":"dl","html":[
                    {"<>":"dt","class":"label","html":"Played"},
                    {"<>":"dd","class":"info","html":"${played}"}
                  ]}
              ]},
            {"<>":"li","html":[
                {"<>":"dl","html":[
                    {"<>":"dt","class":"label","html":"Age"},
                    {"<>":"dd","class":"info","html":"${age} yrs."}
                  ]}
              ]}
          ]},
        {"<>":"div","class":"modify-player","style":"","html":[
            {"<>":"i","class":"bi bi-pencil-fill",
            "data-teamplayerid":"${team_player_id}",
            "data-personplayerid":"${person_player_id}",
            "data-fname":"${f_name}",
            "data-lname":"${l_name}",
            "data-contact":"${contact}",
            "data-image":"${image_path}",
            "data-dob":"${dob}",
            "data-email":"${email}",
            "data-captain":"${is_captain}",
            "data-toggle":"modal","data-target":"#modalPlayerDetails","html":""},
            {"<>":"i","class":"bi bi-trash-fill","html":""}
          ]}
      ]}
  ]};

function loadPlayerList(isScrolling){
    $.post($("base").attr("href")+"Team/players",{
        tid:$("#teamid").val()
    },function(data){
        var response=JSON.parse(data);console.log(response);
        if (response.success === 1) {
           $(".playerListContainer").html("").json2html(response.data,t_playerCard);
        }
    });
}

$('#modalPlayerDetails').on('hide.bs.modal', function (e) {
    $('#modalPlayerDetails input').val("").removeClass("is-valid is-invalid").prop({"checked":false,"disabled":false});
    $('#modalPlayerDetails select').prop("disabled",false);
    $('#modalPlayerDetails option:first').prop('selected',true);
    $(".submitPlayerForm").text("Register");
})
$('#modalPlayerDetails').on('show.bs.modal', function (e) {
    var dataSource = e.relatedTarget.dataset;
    
    $("#tid").val($("#teamid").val());
    
    if (dataSource.teamplayerid !== undefined) {
        $("#pid").val(dataSource.personplayerid);
        $("#ttpid").val(dataSource.teamplayerid);
        $("#registerfName").val(dataSource.fname);
        $("#registerlName").val(dataSource.lname);
        $("#registerEmail").val(dataSource.email);
        $("#registerContact").val(dataSource.contact);
        $("#registerYearOfBirth").val(dataSource.dob);
        if (parseInt(dataSource.captain)) {
            $("#teamAdmin").prop("checked",parseInt(dataSource.captain)).prop("disabled",true);
        }

        $(".submitPlayerForm").text("Update");
    }
});

$(document).on('click','.editPlayer',function(){
    /* var thisPlayer = $(this);
    $('#modalPlayerDetails #id').val(thisPlayer.attr('data-id'));
    $('#modalPlayerDetails #ttpid').val(thisPlayer.attr('data-id'));
    $('#modalPlayerDetails #registerName').val(thisPlayer.attr('data-name'));
    $('#modalPlayerDetails #registerContact').val(thisPlayer.attr('data-contact'));
    $('#modalPlayerDetails #registerEmail').val(thisPlayer.attr('data-email'));

    $('#modalPlayerDetails select').prop("disabled",true);
    $('#modalPlayerDetails #registerTeamSelect').val(thisPlayer.attr('data-teamid'));

    $('#modalPlayerDetails #teamAdmin').prop('checked',parseInt(thisPlayer.attr('data-teamadmin'))); */
})