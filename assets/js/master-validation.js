$(document).on("blur",".onBlurTeamNameUniqueValidate",function(){
    if ($(this).val() !== "") {
        checkIfNameUnique($(this).val(),$(this));
    }
});
function checkIfNameUnique(name,$object){
    $.post($("base").attr("href")+"Team/validateNameUnique",{
        name:name
    },function(data){
        var response = JSON.parse(data);

        if (!response.success) {
            $object.removeClass("is-invalid").addClass("is-valid");
        }else{
            $object.removeClass("is-valid").addClass("is-invalid");
        }
    });
}

$(document).on("blur",".onBlurContactValidate",function(){
    if ($(this).val() !== "") {
        checkContactExist($(this).val(),$(this));
    }
});
function checkContactExist(contact,$object){
    $.post($("base").attr("href")+"Player/validateContact",{
        contact:contact
    },function(data){
        var response = JSON.parse(data);

        if (!response.success) {
            $object.removeClass("is-invalid").addClass("is-valid");
        }else{
            $object.removeClass("is-valid").addClass("is-invalid");
        }
    });
}

$(document).on("blur",".onBlurEmailValidate",function(){
    if ($(this).val() !== "") {
        checkEmailExist($(this).val(),$(this));
    }
});
function checkEmailExist(emailId,$object){
    $.post($("base").attr("href")+"Player/validateEmail",{
        email:emailId
    },function(data){
        var response = JSON.parse(data);

        if (!response.success) {
            $object.removeClass("is-invalid").addClass("is-valid");
        }else{
            $object.removeClass("is-valid").addClass("is-invalid");
        }
    });
}

$(document).on("blur",".onBlurPinCodeValidate",function(){
    if ($(this).val() !== "") {
        checkPinCode($(this).val(),$(this),"validate");
    }
});
function checkPinCode(pinCode,$object,mode){
    
    $.get("https://api.postalpincode.in/pincode/"+pinCode,function(response){

        if (mode === "validate") {
            if (response[0].Status === "Success") {
                $object.removeClass("is-invalid").addClass("is-valid");

                var details = response[0].PostOffice[0];
                $("#locName").val(details.Name);
                $("#locDistrict").val(details.District);
                $("#locState").val(details.State);
                $("#locCountry").val(details.Country);
            }else{
                $(".locHidden").val("");
                $object.removeClass("is-valid").addClass("is-invalid");
            }
        }

        if (mode === "") {
            if (response[0].Status === "Success") {
                return response[0];
            }else{
                return false;
            }
        }
    });
}

$(document).on("change keyup",".keyupNumeric",function(){ 
    var sanitized = $(this).val().replace(/[^0-9]/g, '');
    $(this).val(sanitized);
});

$(document).on("change keyup",".numeric-year",function(e){

    $(this).removeClass("is-invalid");
    var value = $(this).val();

    if (e.type === "keyup" || e.type === "change") {
        if (value.length > 4) {
            //$(this).addClass("is-invalid");
            $(this).val(value.substr(0,4));
        }
    }

    /* if (e.type === "change") {
        if (value.length !== 4) {
            $(this).addClass("is-invalid");
        }
    } */
});
