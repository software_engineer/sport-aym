-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2021 at 08:54 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sport-aym`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_field_position`
--

DROP TABLE IF EXISTS `tbl_field_position`;
CREATE TABLE `tbl_field_position` (
  `id` int(11) NOT NULL,
  `position_id` varchar(255) NOT NULL,
  `position_name` varchar(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tbl_field_position`
--

TRUNCATE TABLE `tbl_field_position`;
--
-- Dumping data for table `tbl_field_position`
--

INSERT INTO `tbl_field_position` (`id`, `position_id`, `position_name`, `created_on`, `updated_on`, `modified_by`) VALUES
(1, '1', 'Goalkeeper', '2021-09-16 10:09:28', '2021-09-16 10:09:28', '1'),
(2, '2', 'Defender', '2021-09-16 10:09:28', '2021-09-16 10:09:28', '1'),
(3, '3', 'Midfielder', '2021-09-16 10:09:28', '2021-09-16 10:09:28', '1'),
(4, '4', 'Forward', '2021-09-16 10:09:28', '2021-09-16 10:09:28', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_person`
--

DROP TABLE IF EXISTS `tbl_person`;
CREATE TABLE `tbl_person` (
  `id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `dob` year(4) DEFAULT NULL,
  `image_path` varchar(255) NOT NULL,
  `person_type` varchar(255) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tbl_person`
--

TRUNCATE TABLE `tbl_person`;
--
-- Dumping data for table `tbl_person`
--

INSERT INTO `tbl_person` (`id`, `f_name`, `l_name`, `contact`, `email`, `dob`, `image_path`, `person_type`, `updated_on`, `created_on`, `created_by`) VALUES
(1, 'Jenifer', 'Aniston', '9859493049', 'jenann@gmail.com', 1992, '', '3', '2021-09-12 18:45:20', '2021-09-12 18:45:20', '1'),
(3, 'Joey', 'Tribbiani', '9887867565', 'joey@friends.com', 1998, '', '3', '2021-09-15 07:58:40', '2021-09-15 07:58:40', '1'),
(6, 'Jenny', 'Marbles', '9182736475', 'jenmar@gmail.com', 1998, '', '3', '2021-10-07 17:32:14', '2021-09-16 07:15:21', '3'),
(13, 'Jimmy', 'Carter', '9988776654', 'jim@gmail.com', 1989, '', '3', '2021-10-07 20:13:16', '2021-09-16 07:47:40', '3'),
(14, 'Amy', 'Winehouse', '3388447755', 'amywine@gmail.com', 1995, '', '3', '2021-09-16 09:39:51', '2021-09-16 08:01:13', '3'),
(15, 'Carson', 'Clay', '9988776655', 'claycarson@gmail.com', 1993, '', '3', '2021-10-07 20:15:51', '2021-10-07 20:15:51', '3'),
(16, 'Captain', 'Jack', '8899776655', 'jackcap@yahoo.com', 1998, '', '3', '2021-10-07 20:16:34', '2021-10-07 20:16:34', '3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_person_type`
--

DROP TABLE IF EXISTS `tbl_person_type`;
CREATE TABLE `tbl_person_type` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tbl_person_type`
--

TRUNCATE TABLE `tbl_person_type`;
--
-- Dumping data for table `tbl_person_type`
--

INSERT INTO `tbl_person_type` (`id`, `type`, `code`) VALUES
(1, 'admin', '1'),
(2, 'event coordinator', '2'),
(3, 'player', '3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teams`
--

DROP TABLE IF EXISTS `tbl_teams`;
CREATE TABLE `tbl_teams` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `location_name` varchar(255) NOT NULL,
  `location_district` varchar(255) DEFAULT NULL,
  `location_state` varchar(255) DEFAULT NULL,
  `location_country` varchar(255) DEFAULT NULL,
  `location_pincode` varchar(255) DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(255) DEFAULT NULL,
  `img_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tbl_teams`
--

TRUNCATE TABLE `tbl_teams`;
--
-- Dumping data for table `tbl_teams`
--

INSERT INTO `tbl_teams` (`id`, `name`, `location_name`, `location_district`, `location_state`, `location_country`, `location_pincode`, `updated_on`, `created_on`, `created_by`, `img_name`) VALUES
(1, 'Aniston', 'Bandra West', 'Mumbai', 'Maharashtra', 'India', '400050', '2021-09-12 18:45:20', '2021-09-12 18:45:20', '1', '12092021204520_test-inviteUser.jpg'),
(3, 'Tribbiani', 'Bazargate', 'Mumbai', 'Maharashtra', 'India', '400001', '2021-09-15 07:58:41', '2021-09-15 07:58:41', '1', '15092021095841_JoeyTribbiani.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_team_players`
--

DROP TABLE IF EXISTS `tbl_team_players`;
CREATE TABLE `tbl_team_players` (
  `id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `player_id` varchar(255) NOT NULL,
  `is_captain` tinyint(1) DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tbl_team_players`
--

TRUNCATE TABLE `tbl_team_players`;
--
-- Dumping data for table `tbl_team_players`
--

INSERT INTO `tbl_team_players` (`id`, `team_id`, `player_id`, `is_captain`, `updated_on`, `created_on`, `created_by`) VALUES
(1, 1, '1', 0, '2021-09-16 08:01:13', '2021-09-12 18:45:20', '1'),
(3, 3, '3', 1, '2021-09-15 10:31:17', '2021-09-15 07:58:41', '1'),
(5, 1, '6', 0, '2021-09-16 08:01:13', '2021-09-16 07:15:21', '1'),
(12, 1, '13', 0, '2021-10-07 20:11:14', '2021-09-16 07:47:40', '1'),
(13, 1, '14', 0, '2021-10-07 20:16:34', '2021-09-16 08:01:13', '1'),
(14, 1, '15', 0, '2021-10-07 20:16:34', '2021-10-07 20:15:51', '1'),
(15, 1, '16', 1, '2021-10-07 20:16:34', '2021-10-07 20:16:34', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_field_position`
--
ALTER TABLE `tbl_field_position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_person`
--
ALTER TABLE `tbl_person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_person_type`
--
ALTER TABLE `tbl_person_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_teams`
--
ALTER TABLE `tbl_teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_team_players`
--
ALTER TABLE `tbl_team_players`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_field_position`
--
ALTER TABLE `tbl_field_position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_person`
--
ALTER TABLE `tbl_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_person_type`
--
ALTER TABLE `tbl_person_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_teams`
--
ALTER TABLE `tbl_teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_team_players`
--
ALTER TABLE `tbl_team_players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
