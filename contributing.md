# Contributing to CodeIgniter

CodeIgniter is a community driven project and accepts contributions of code and documentation from the community. These contributions are made in the form of Issues or [Pull Requests](http://help.github.com/send-pull-requests/) on the [CodeIgniter repository](https://github.com/bcit-ci/CodeIgniter) on GitHub.

Issues are a quick way to point out a bug. If you find a bug or documentation error in CodeIgniter then please check a few things first:

1. There is not already an open Issue
2. The issue has already been fixed (check the develop branch, or look for closed Issues)
3. Is it something really obvious that you can fix yourself?

Reporting issues is helpful but an even better approach is to send a Pull Request, which is done by "Forking" the main repository and committing to your own copy. This will require you to use the version control system called Git.

## Guidelines

Before we look into how, here are the guidelines. If your Pull Requests fail
to pass these guidelines it will be declined and you will need to re-submit
when you’ve made the changes. This might sound a bit tough, but it is required
for us to maintain quality of the code-base.

### PHP Style

All code must meet the [Style Guide](https://codeigniter.com/user_guide/general/styleguide.html), which is
essentially the [Allman indent style](https://en.wikipedia.org/wiki/Indent_style#Allman_style), underscores and readable operators. This makes certain that all code is the same format as the existing code and means it will be as readable as possible.

### Heroku deployment

[How to Deploy a PHP and MySQL site to Heroku and configure the database with PHPMyAdmin](https://www.doabledanny.com/Deploy-PHP-And-MySQL-to-Heroku)

## Heroku Pipeline
[Build a CI/CD pipeline](https://dev.to/mwanjemike/build-a-ci-cd-pipeline-with-heroku-ci-3de9?fbclid=IwAR2U0S3si1iZyUl0w0HuoPoxToQ3Tbv_wb_Y0mfd1xUkDvXGWiIglLzMbd4)